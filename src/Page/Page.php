<?php

	namespace Page;

	class Page{

		private $curPage;      //当前页
        private $listRows;     //每页显示记录数
        private $totalRows;    //总记录数
        private $totalPage;    //总页码
        private $pageNum;      //每页显示的页码
        private $startRows;    //分页开始行
        private $url;          //连接地址
        private $pageHtml;     //页码的html
        private $parameter;    //查询参数
		
		public function __construct($url, $totalRows, $curPage, $parameter=array(), $listRows=10, $pageNum=5 )
		{

            $this->curPage = is_numeric($curPage) ? $curPage : 1;
			
			$this->listRows = $listRows;
			
			$this->totalRows = $totalRows;
			
			$this->pageNum = $pageNum;
			
			$this->startRows = $this->listRows * ($this->curPage-1);
			
			$this->startRows = $this->startRows > $this->totalRows ? 0 :$this->startRows;

            $this->parameter = empty($parameter) ? '' : '?'.http_build_query($parameter);

            $this->url = is_numeric(substr(strrchr($url,'/'), 1)) ? substr($url, 0, strrpos($url,'/')) : $url;

		}

        /**
         * 获取开始行
         * @return mixed
         */
        public function getStartRows()
        {
            return $this->startRows;
        }

        /**
         * 获取每页多少行
         * @return int  $this->listRows
         */
        public function getListRows()
        {
            return $this->listRows;
        }

        /**
         * 获取分页页码信息
         * @return string
         */
		public function getPageHtml(){
			//如果查询记录为零则返回空
			if(0 == $this->totalRows){
				return '';
			}
			//总的页码
			$this->totalPage = ceil($this->totalRows / $this->listRows);
			
			//当前页
			$this->curPage = $this->curPage < 1 ? 1 :$this->curPage;
			$this->curPage = $this->curPage > $this->totalPage ? $this->totalPage : $this->curPage;
			
			//计算开始页码
			$firstPage = $this->curPage - floor($this->pageNum/2);
			$firstPage = $firstPage < 1 ? 1 : $firstPage;
			
			//计算结束页码
			$endPage = $this->curPage + floor($this->pageNum/2);
			$endPage = $endPage > $this->totalPage ? $this->totalPage : $endPage;
			
			//显示的页码数不够时  即  总页码  - 当前页 < 每页显示的页码
			$tempPageNum = $endPage - $firstPage + 1;
				
			if($tempPageNum < $this->pageNum  && $firstPage>1 ){
				$firstPage = $firstPage -($this->pageNum - $this->curPage);
				$firstPage = ($this->totalPage-$this->pageNum+1) >= $firstPage ? $firstPage : ($this->totalPage-$this->pageNum+1);
				$firstPage = $firstPage < 1 ? 1 : $firstPage;
				$tempPageNum = $endPage - $firstPage + 1;
			}
			
			if($tempPageNum < $this->pageNum && $endPage <$this->totalPage ){
				$endPage = $endPage + ($this->pageNum-$tempPageNum);
				$endPage = $endPage > $this->totalPage ? $this->totalPage : $endPage;
			}
			
			$this->pageHtml ='';
				
			if($this->curPage>1){
				$this->pageHtml .='<a title="上一页" href="' . $this->url . '/' . ($this->curPage-1).$this->parameter.'">上一页</a>'. PHP_EOL;
			} else {
                $this->pageHtml .= '<em>上一頁</em>'. PHP_EOL;
            }
			
			for($i=$firstPage ; $i<=$endPage;$i++){
				if($i == $this->curPage ){
					$this->pageHtml .= '<strong>'.$i.'</strong>'. PHP_EOL;
				}else{
					$this->pageHtml .= '<a title ="第'. $i .'頁"  href="' . $this->url . '/' . $i . $this->parameter.'">'.$i.'</a>' . PHP_EOL;
				}
			}
			
			if($this->curPage<$endPage){
				$this->pageHtml.= '<a title="下一页" href="'.$this->url.'/'.($this->curPage+1).$this->parameter.'">下一页</a>';
			} else {
                $this->pageHtml .= '<em>下一页</em>';
            }
			
			return $this->pageHtml;
		}
		
	}
	
?>