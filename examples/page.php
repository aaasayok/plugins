<?php
/**
 * Created by PhpStorm.
 * User: GIGABYTE
 * Date: 2015/6/2
 * Time: 9:20
 */

    include __DIR__ . '/../vendor/autoload.php';

    header('Content-type:text/html;charset=utf-8');

    $curPage = $_SERVER['PATH_INFO'] ? ltrim($_SERVER['PATH_INFO'], '/') : 1;

    $url = "http://". $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME'];

    $page = new \Page\Page($url, 100, $curPage);

    $pagination = $page->getPageHtml();

    echo $pagination;

?>